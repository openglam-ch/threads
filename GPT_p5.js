let pointPositions = [];
let points = [];
let lines = [];
const maxTotalLines = 3; // Maximum total number of lines
let currentLine = null;
let lastConnectedPoint = null;
const numPoints = 10;
const textOffset = 20; // Offset for text positioning
const maxLinesPerPoint = 3; // Maximum number of lines between points
let lineColor; // Variable to store the selected line color

const characterNames = [
  "Frodo",
  "Aragorn",
  "Gandalf",
  "Legolas",
  "Gimli",
  "Samwise",
  "Boromir",
  "Merry",
  "Pippin",
  "Galadriel",
];

function setup() {
  createCanvas(800, 600);
  // Create an oval-shaped form
  let ovalWidth = 400;
  let ovalHeight = 300;
  let centerX = width / 2;
  let centerY = height / 2;

  // Create points with names and a visit counter inside the oval
  for (let i = 0; i < numPoints; i++) {
    let angle = TWO_PI / numPoints * i;
    let x = centerX + ovalWidth / 2 * cos(angle) * 0.8; // Adjust the factor (0.8) to place points inside the oval
    let y = centerY + ovalHeight / 2 * sin(angle) * 0.8; // Adjust the factor (0.8) to place points inside the oval
    pointPositions.push(createVector(x, y));
    points.push({
      position: pointPositions[i],
      name: characterNames[i],
      visits: 0,
      connectedPoints: [], // Keep track of connected points
    });
  }

  textSize(18);
  textAlign(CENTER, CENTER);
  textFont('Arial');

  // Create color buttons
  let colorButtons = [];
  let colors = ['red', 'blue', 'green'];
  let buttonWidth = width / 3;
  for (let i = 0; i < 3; i++) {
    colorButtons.push(createButton(colors[i]));
    colorButtons[i].position(i * buttonWidth, height + 10);
    colorButtons[i].size(buttonWidth, 30);
    colorButtons[i].mousePressed(() => {
      lineColor = colors[i];
    });
  }
  lineColor = colors[0]; // Set initial line color
}

function draw() {
  background(240);

  // Display oval-shaped form
  noFill();
  stroke(0);
  strokeWeight(2); // Set a constant line thickness for the oval
  ellipse(width / 2, height / 2, 400, 300);

  // Display all points
  strokeWeight(10);
  stroke(0);
  for (let charPoint of points) {
    point(charPoint.position.x, charPoint.position.y);
  }

  // Display existing lines with hair-like texture
  for (let line of lines) {
    line.show();
  }

  // Display the current line being drawn (if any) with hair-like texture
  if (currentLine) {
    currentLine.setColor(lineColor);
    currentLine.show();
  }

  // Display point names
  noStroke();
  fill(0);
  for (let charPoint of points) {
    text(charPoint.name, charPoint.position.x, charPoint.position.y - textOffset);
  }
}

function mousePressed() {
  if (lines.length >= maxTotalLines) {
    return; // Do not draw more lines if the maximum total lines are reached
  }

  for (let i = 0; i < numPoints; i++) {
    let charPoint = points[i];
    if (dist(mouseX, mouseY, charPoint.position.x, charPoint.position.y) < 10) {
      if (!currentLine && charPoint.visits < maxLinesPerPoint) {
        currentLine = new Line(charPoint.position.copy(), null);
        lastConnectedPoint = charPoint.position.copy();
        charPoint.visits++;
        charPoint.connectedPoints.push(lastConnectedPoint);
      } else if (lastConnectedPoint !== charPoint.position && charPoint.visits < maxLinesPerPoint &&
                 !charPoint.connectedPoints.includes(lastConnectedPoint)) {
        currentLine.end = charPoint.position.copy();
        currentLine.color = lineColor;
        lines.push(currentLine);
        currentLine = new Line(charPoint.position.copy(), null);
        lastConnectedPoint = charPoint.position.copy();
        charPoint.visits++;
        charPoint.connectedPoints.push(lastConnectedPoint);
        if (charPoint.visits >= maxLinesPerPoint) {
          noLoop(); // Stop drawing lines from this point after reaching the maximum
        }
      }
      break;
    }
  }
}

class Line {
  constructor(start, end) {
    this.start = start.copy();
    this.end = end ? end.copy() : null;
    this.color = 'red'; // Default line color
  }

  setColor(color) {
    this.color = color;
  }

  show() {
    stroke(this.color);
    strokeWeight(3); // Set the stroke weight for hair-like lines
    if (this.end) {
      // Simulate hair-like texture with random deviations
      let numSegments = int(dist(this.start.x, this.start.y, this.end.x, this.end.y) / 5);
      for (let i = 0; i <= numSegments; i++) {
        let t = i / numSegments;
        let x1 = lerp(this.start.x, this.end.x, t);
        let y1 = lerp(this.start.y, this.end.y, t);
        let x2 = x1 + random(-2, 2); // Add random deviations to x
        let y2 = y1 + random(-2, 2); // Add random deviations to y
        line(x1, y1, x2, y2);
      }
    }
  }
}
